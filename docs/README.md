# Billion Dollar Project
### Background
This repository contains a simple program written in Golang named `pinger`. This service reponds with `"hello world"` at the root path and can be configured to ping another server through the environment variables (see the file at `./cmd/pinger/config.go`). By default, running it will make it start a server and ping itself.

### Prerequisites:
You will need the following installed:
-   `go` to run the application (check with `go version`)
-   `docker` for image building/publishing (check with `docker version`)
-   `docker-compose` for environment provisioning (check with `docker-compose version`)
-   `git` for source control (check with `git -v`)
-   `make` for simple convenience scripts (check with `make -v`)
- gitlab account for pulling this repo

### Directory Structure
| Directory  	  | Description  |
|-----------------|--------------|
| `/bin`  	 	  |   Contains binaries			|
| `/cmd`  		  |   Contains source code for CLI interfaces|
| `/deployments`  |   Contains image files and manifests for deployments|
| `/docs`  		  |   Contains documentation|
| `/vendor`  	  |   Contains dependencies (use `make dep` to populate it)|

### Usage
Note: all command are to be executed from the root of this folder!
|task| command  |
|--|--|
| create dependencies | `make dep` |
| create binary | `make build` |
| run go code | `make run` |
| run go tests | `make test` |
| build docker image | `make docker_image` |
| run single docker container | `make docker_testrun` |
| run multiple docker containers | `make testenv` |
| package docker image into tar | `make docker_tar` |
| unpackage docker image from tar | `make docker_untar` |

## Devops Pipeline

### The CI pipeline generates 2 artifacts:
- pinger binary
- docker image (in the form of tar package)

Note: 
For the latest artifacts, please download these artifacts from the CI/CD page by clicking on "CI/CD" tab on the left pane and click on download button on the latest passing pipeline

### Alternative Method for Getting Docker Image
Prerequisites: Permission for pulling images from docker registry
- Navigate to https://gitlab.com/billion_dollar_projects/billion_dollar_project/container_registry
- Select the corresponding image and docker pull <image_name>:<image_tag>
- successfully built docker images are currently versioned by git tag for ease of tracking exact codebase used to build the image 

### For DevOps Engineer
### Read, Study, Understand:
- How we build docker image: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker
- Sample yaml for building docker image: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml 
- How we build golang binary: https://golang.org/cmd/go/#hdr-Compile_packages_and_dependencies
- Build binary using golang image: https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#define-image-and-services-from-gitlab-ciyml  